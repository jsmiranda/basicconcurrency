import org.junit.Test;

import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class BankTest {
    @Test(timeout = Bank.SLEEP_TIME_MILLIS * 3 - 100) // if the testConcurrentAddBalance fails with timeout, it's likely to be a concurrency problem (locking more than expected)
    public void testConcurrentAddBalance() throws Exception {
        Bank bank = new Bank();

        // assert initial state
        assertEquals(0, bank.getBalance("Pedro"));
        assertEquals(0, bank.getBalance("Joao"));

        // exercise method under test
        final CompletableFuture<Void> fut1 = CompletableFuture.runAsync(() -> bank.addBalance(10, "Pedro"));
        final CompletableFuture<Void> fut2 = CompletableFuture.runAsync(() -> bank.addBalance(10, "Pedro"));
        final CompletableFuture<Void> fut3 = CompletableFuture.runAsync(() -> bank.addBalance(10, "Joao"));
        final CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(fut1, fut2, fut3);

        // wait for the threads to finish
        combinedFuture.get();

        // final assertions
        assertEquals(20, bank.getBalance("Pedro"));
        assertEquals(10, bank.getBalance("Joao"));
    }
}