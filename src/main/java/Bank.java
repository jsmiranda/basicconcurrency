import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Bank {

    /**
     * Time that to sleep while adding balance. Used for testing.
     */
    public static final int SLEEP_TIME_MILLIS = 2000;

    /**
     * Accounts are a pair of a name (unique identifier) and the respective balance.
     *
     * Implemented as an in=memory map for demo purposes only.
     *
     * The best way to implement these accounts would be with an {@code Account} object, but that would almost defeat
     * the purpose of this demo, because then we could just synchronize methods of these {@code Account} objects
     * instead of using explicit {@code synchronized} blocks.
     */
    private final Map<String, Integer> accounts = new ConcurrentHashMap<>();

    /**
     * Adds the given balance to the account identified by the given name.
     *
     * @param amount The amount to add.
     * @param name The name that identifies the account to which we want to add balance.
     */
    public void addBalance(final int amount, final String name) {
        // DISCLAIMER:
        // The best way to implement these accounts would be with an {@code Account} object, but that would almost defeat
        // the purpose of this demo, because then we could just synchronize methods of these {@code Account} objects
        // instead of using explicit {@code synchronized} blocks.
        // Another possibility would be to use computeIfAbsent so that all computing is run in mutual exclusion.

        // This implementation is probably the one that is both correct and easier to understand for someone that is not
        // yet used to Concurrent APIs

        // Below we need to force a call to Integer constructor instead of relying in auto-boxing because of
        // Java Integer cache (see https://stackoverflow.com/a/3131208).
        // We shouldn't ever use Integers' monitors for that reason. Just doing it because it's a simple demo.
        accounts.putIfAbsent(name, new Integer(0));
        final Integer lock = accounts.get(name);

        synchronized (lock) { // TODO: Remove the synchronized to see test failing
            final Integer oldBalance = accounts.get(name); // now that we acquired the lock, we must get the value again instead of reusing "lock" variable
            final int newBalance = oldBalance + amount;

            // Let's sleep a bit so that we increase chance of having another thread trying to manipulate this account
            // simultaneously. Just for tests. Note: if we wanted to enable and disable this feature dynamically we could
            // extract a method that would have an empty implementation in development, and the sleep only in testing (e.g.
            // via mocking).
            try {
                Thread.sleep(SLEEP_TIME_MILLIS);
            } catch (final InterruptedException e) {
                throw new IllegalStateException("Unexpected exception. This thread must not be interrupted.", e);
            }

            accounts.put(name, newBalance);
        }
    }

    /**
     * Gets the balance of the account identified by the given name.
     *
     * @param name The name that identifies the account.
     */
    public int getBalance(final String name) {
        return accounts.getOrDefault(name, 0);
    }
}