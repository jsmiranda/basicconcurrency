**Purpose**

This is a toy project to teach basic concurrency stuff in Java. It may evolve to functional programming as well. The main goal is to provide a base for people to test changes and see what happens.

## How to run

Import it as a maven project, make your changes if you wish, and then run the tests with `mvn test`